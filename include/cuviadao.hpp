#ifndef CUVIADAO_H
#define CUVIADAO_H

#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <eosio/singleton.hpp>
#include <eosio/multi_index.hpp>
#include <eosio/transaction.hpp>

#include "trail.hpp"

using namespace eosio;
using std::string;

CONTRACT cuviadao : public contract {
   public:
      using contract::contract;

      struct [[eosio::table, eosio::contract("cuviadao") ]] DAOConfig 
      {
         name           trail_contract          = name("trailservice");
         name           last_ballot_id          = name("cuvia1");
      };

      typedef singleton<"config"_n, DAOConfig> config_table;
      typedef multi_index<"config"_n, DAOConfig> config_table_placehoder;

      struct [[eosio::table, eosio::contract("cuviadao") ]] Member 
      {
         name           member                  ;
         uint64_t       primary_key() const { return member.value; }
      };

      typedef multi_index<"members"_n, Member> member_table;

      struct [[eosio::table, eosio::contract("cuviadao") ]] Applicant 
      {
         name           applicant                  ;
         string         content                    ;
         
         time_point  created_date = current_time_point();
         time_point  updated_date = current_time_point();

         uint64_t       primary_key() const { return applicant.value; }
      };
      typedef multi_index<"applicants"_n, Applicant> applicant_table;

      // scope: proposal type (name)
      struct [[eosio::table, eosio::contract("cuviadao") ]] Proposal
      {
         uint64_t                   id                ;
         
         // core maps
         map<string, name>          names             ;
         map<string, string>        strings           ;
         map<string, asset>         assets            ;
         map<string, time_point>    time_points       ;
         map<string, uint64_t>      ints              ;
         map<string, transaction>   trxs              ;
         map<string, float>         floats            ;
         uint64_t                   primary_key()     const { return id; }

         // indexes
         name           proposer                ; 
         uint64_t       by_proposer()           const { return proposer.value; }

         time_point                 created_date    = current_time_point();
         time_point                 updated_date    = current_time_point();
         uint64_t    by_created () const { return created_date.sec_since_epoch(); }
         uint64_t    by_updated () const { return updated_date.sec_since_epoch(); }
      };

      typedef multi_index<"proposals"_n, Proposal,
         indexed_by<"bycreated"_n, const_mem_fun<Proposal, uint64_t, &Proposal::by_created>>,
         indexed_by<"byupdated"_n, const_mem_fun<Proposal, uint64_t, &Proposal::by_updated>>,
         indexed_by<"byproposer"_n, const_mem_fun<Proposal, uint64_t, &Proposal::by_proposer>>
      > proposal_table;

      struct [[eosio::table, eosio::contract("cuviadao") ]] Debug
      {
         uint64_t    debug_id;
         string      notes;
         time_point  created_date = current_time_point();
         uint64_t    primary_key()  const { return debug_id; }
      };

      typedef multi_index<"debugs"_n, Debug> debug_table;

      /*
         Proposal table scoped to "payouts", the proposal type

         Proposal attributes: 
            - type: int
               - "status"
               - "start_period"
               - "end_period"
            
            - type: name
               - "recipient"
               - "ballot_id": used to interface to Trail, populated by system

            - type: string
               - "title"  
               - "description"
               - "content"

            - type: asset
               - "cuvia_amount"
               - "tlos_amount"

            - type: time_point_sec
               - "created_date"
               - "updated_date"

            - type: transaction
               - "transaction"
      */
     ACTION propose (const name&                		proposer, 
                     const name&                      proposal_type,
                     const std::optional<name>&       trx_action_name,
                     const map<string, name> 		   names,
                     const map<string, string>        strings,
                     const map<string, asset>         assets,
                     const map<string, time_point>    time_points,
                     const map<string, uint64_t>      ints,
                     const map<string, float>         floats,
                     const map<string, transaction>   trxs);
      
      ACTION apply (const name&     applicant, const string& content);

      ACTION enroll (const name& enroller,
                     const name& applicant, 
					      const string& content);

      // Admin
      ACTION reset ();
      ACTION eraseprop (const name&       proposal_type,
                        const uint64_t&   proposal_id);

      ACTION settrail (const name&    trail_contract);
      ACTION setlastballt (const name& last_ballot_id);

      // These actions are executed only on approval of a proposal. 
      // To introduce a new proposal type, we would add another action to the below.
      ACTION makepayout (  const uint64_t&   proposal_id);
      
      // anyone can call closeprop, it executes the transaction if the voting passed
      ACTION closeprop(const name& proposal_type, const uint64_t& proposal_id);
         
      // temporary hack - keep a list of the members, although true membership is governed by token holdings
      ACTION removemember(const name& member_to_remove);
      ACTION addmember (const name& member);
      
   private:
      const symbol S_CUV = symbol ("CUV", 4);

      void defcloseprop (const uint64_t& proposal_id);
      void qualify_proposer (const name& proposer);
      name register_ballot (const name& proposer, 
								      const map<string, string>& strings);

      void debug (const string& notes) {
         debug_table d_t (get_self(), get_self().value);
         d_t.emplace (get_self(), [&](auto &d) {
            d.debug_id = d_t.available_primary_key();
            d.notes = notes;
         });
      }

      void archive (Proposal& prop) {
         proposal_table p_t_archive (get_self(), "archive"_n.value);
	      p_t_archive.emplace (get_self(), [&](auto &p) {
            p.id                          = p_t_archive.available_primary_key();
            p.proposer                    = prop.proposer;
            p.names                       = prop.names;
            p.assets                      = prop.assets;
            p.time_points                 = prop.time_points;
            p.ints                        = prop.ints;
            p.ints["prop_id_when_open"]   = prop.id;  // id of archived proposal may not match id open same proposal when opened because they use different scopes
            p.trxs                        = prop.trxs;
	      });
      }
};

#endif