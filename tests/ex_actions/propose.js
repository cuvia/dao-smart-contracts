const { Api, JsonRpc } = require("eosjs");
const { JsSignatureProvider } = require("eosjs/dist/eosjs-jssig");
const fetch = require("node-fetch");
const { TextEncoder, TextDecoder } = require("util");

const defaultPrivateKey = process.env.PRIVATE_KEY;
const signatureProvider = new JsSignatureProvider([defaultPrivateKey]);

const rpc = new JsonRpc("https://test.telos.kitchen", { fetch });

const api = new Api({
  rpc,
  signatureProvider,
  textDecoder: new TextDecoder(),
  textEncoder: new TextEncoder()
});

const sendTrx = async () => {

  try {
    const actions = [{
      account: "cuviadaomain",
      name: "propose",
      authorization: [
        {
          actor: "johnnycuvia1",
          permission: "active"
        }
      ],
      data: {
        proposer: "johnnycuvia1",
        proposal_type: "payouts",
        trx_action_name: "makepayout",
        names: [
          {
            "key":"recipient",
            "value":"johnnycuvia1"
          }
        ],
        strings: [
        {
          "key": "name",
          "value": "Financial Analysis Project"
        },
        {
          "key": "description",
          "value": "This project is important and should be approved."
        },
        {
          "key": "content",
          "value": "This supports markdown for *bolding* and formatting content."
        }
        ],
        assets: [
        {
          "key": "cuvia_amount",
          "value": "4.5000 CUV"
        },
        {
          "key": "tlos_amount",
          "value": "1.1122 TLOS"
        }
        ],
        time_points: [],
        ints: [],
        floats: [],
        trxs: []
      }
    }
    ]

    const result = await api
      .transact(
        {
          actions: actions
        },
        {
          blocksBehind: 3,
          expireSeconds: 30
        }
      )

    console.log("Successfull : ", result);
  } catch (e) {
    console.error(e)
    console.log('Please, fix an error and run script again')
    process.exit(1);
  }
}

const main = async () => {
  await sendTrx()
}

main()
