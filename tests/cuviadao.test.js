/* eslint-disable prettier/prettier */
const eoslime = require("../../eoslime").init("local");
const assert = require('assert');

const CUVIADAO_WASM = "../cuviadac/cuviadao/cuviadao/cuviadao.wasm";
const CUVIADAO_ABI = "../cuviadac/cuviadao/cuviadao/cuviadao.abi";
const EOSIOTOKEN_WASM = "../cuviadac/cuviadao/eosiotoken/eosiotoken.wasm";
const EOSIOTOKEN_ABI = "../cuviadac/cuviadao/eosiotoken/eosiotoken.abi";
const TRAIL_WASM = "../telos.contracts/eosio.trail/eosio.trail.wasm";
const TRAIL_ABI = "../telos.contracts/eosio.trail/eosio.trail.abi";

describe("HyphaDAC Testing", function() {
  this.timeout(150000);

  let cuviadaoContract, eosTokenContract, trailContract;
  let cuviadaoAccount, eosTokenAccount, trailAccount;
  let member1, member2, member3, member4, member5;
  let accounts;

  before(async () => {
    accounts = await eoslime.Account.createRandoms(10);
    cuviadaoAccount = accounts[0];
    eosTokenAccount = accounts[1];
    member1 = accounts[2];
    member2 = accounts[3];
    member3 = accounts[4];
    member4 = accounts[5];
    member5 = accounts[6];
    trailAccount = accounts[7];
    cuviaBoardAccount = accounts[8];

    console.log(" Hypha DAC Account     : ", cuviadaoAccount.name);
    console.log(" Token Account         : ", eosTokenAccount.name);
    console.log(" Hypha Board Contract  : ", cuviaBoardAccount.name);
    console.log(" Trail Service         : ", trailAccount.name);
    console.log(" Member 1              : ", member1.name);
    console.log(" Member 2              : ", member2.name);
    console.log(" Member 3              : ", member3.name);
    console.log(" Member 4              : ", member4.name);
    console.log(" Member 5              : ", member5.name);

    await cuviadaoAccount.addPermission(
      cuviadaoAccount.name,
      "active",
      cuviadaoAccount.name,
      "eosio.code"
    );
    // await cuviaBoardAccount.addPermission(
    //   cuviaBoardAccount.name,
    //   "active",
    //   cuviaBoardAccount.name,
    //   "eosio.code"
    // );
    await trailAccount.addPermission(
      trailAccount.name,
      "active",
      trailAccount.name,
      "eosio.code"
    );

    cuviadaoContract = await eoslime.AccountDeployer.deploy(
      CUVIADAO_WASM,
      CUVIADAO_ABI,
      cuviadaoAccount
    );
    eosTokenContract = await eoslime.AccountDeployer.deploy(
      EOSIOTOKEN_WASM,
      EOSIOTOKEN_ABI,
      eosTokenAccount
    );
    // cuviaBoardContract = await eoslime.AccountDeployer.deploy(
    //   BOARD_WASM,
    //   BOARD_ABI,
    //   cuviaBoardAccount
    // );
    trailContract = await eoslime.AccountDeployer.deploy(
      TRAIL_WASM,
      TRAIL_ABI,
      trailAccount
    );

    console.log(" Set config on cuvia dao")
    await cuviadaoContract.setconfig(eosTokenAccount.name, trailAccount.name, {from: cuviadaoAccount });
    console.log("initialize cuvia dao")
    await cuviadaoContract.init({ from: cuviadaoAccount });
    // await cuviaBoardContract.setconfig(member1.name, { from: cuviaBoardAccount});
    // await cuviaBoardContract.inittfvt("https://joinseeds.com", { from: cuviaBoardAccount })
    // await cuviaBoardContract.inittfboard("https://joinseeds.com", { from: cuviaBoardAccount })

    console.log ("issugin tokens to members");
    await trailContract.issuetoken(cuviadaoContract.name, member1.name, "1 HVOICE", 0, { from: cuviadaoAccount })
    await trailContract.issuetoken(cuviadaoContract.name, member2.name, "1 HVOICE", 0, { from: cuviadaoAccount })
    await trailContract.issuetoken(cuviadaoContract.name, member3.name, "1 HVOICE", 0, { from: cuviadaoAccount })
    await trailContract.issuetoken(cuviadaoContract.name, member4.name, "1 HVOICE", 0, { from: cuviadaoAccount })
    await trailContract.issuetoken(cuviadaoContract.name, member5.name, "1 HVOICE", 0, { from: cuviadaoAccount })
      console.log ("comleted issuance")

    console.log (" Creating CUVIA and PRESEED tokens...")
    await eosTokenContract.create(cuviadaoAccount.name, "100000000 CUVIA");
    // await eosTokenContract.create(cuviadaoAccount.name, "100000000 HVOICE");
    await eosTokenContract.create(cuviadaoAccount.name, "100000000 PRESEED");
  });

  it("Should create a new role", async () => {

    // await cuviadaoContract.nominate(member1.name, member1.name, { from: member1 })
    // await cuviadaoContract.makeelection(member1.name, "string", { from: member1 })
    // await cuviadaoContract.addcand(member1.name, "string", {from: member1 })
    
    await cuviadaoContract.proposerole(
      member1.name,
      "blockchdev",
      "https://joinseeds.com",
      "Blockchain developer",
      "10 CUVIA",
      "10.00000000 PRESEED",
      "10 HVOICE", { from: member1 }
    );
    await cuviadaoContract.proposerole(
      member1.name,
      "websitedev",
      "https://joinseeds.com",
      "Website developer/maintainer",
      "8 CUVIA",
      "10.00000000 PRESEED",
      "8 HVOICE", { from: member1 }
    );

    const roleprops = await cuviadaoContract.provider.eos.getTableRows({
      code: cuviadaoAccount.name,
      scope: cuviadaoAccount.name,
      table: "roleprops",
      json: true
    });
    assert.equal(roleprops.rows.length, 2);
    console.log(roleprops);
  });

  it("Should propose to assign a user to a role", async () => {
    await cuviadaoContract.propassign(member1.name, member1.name, 
        0, "https://joinseeds.com", "Description", 1, 0.500000000000, { from: member1 });
    await cuviadaoContract.propassign(member2.name, member2.name, 
      1, "https://joinseeds.com", "Description", 2, 1.000000000000, { from: member2 });

    const assprops = await cuviadaoContract.provider.eos.getTableRows({
      code: cuviadaoAccount.name,
      scope: cuviadaoAccount.name,
      table: "assprops",
      json: true
    });
    assert.equal(assprops.rows.length, 2);
    console.log(assprops);
  });

  it("Should create a contribution", async () => {
    await cuviadaoContract.proppayout(
      member5.name,
      member5.name,
      "Purchased conference fees for Rise event",
      "https://joinseeds.com",
      "965 CUVIA",
      "965.00000000 PRESEED",
      "45 HVOICE",
      1000, { from: member5 }
    );

    const payoutprops = await cuviadaoContract.provider.eos.getTableRows({
      code: cuviadaoAccount.name,
      scope: cuviadaoAccount.name,
      table: "payoutprops",
      json: true
    });
    assert.equal(payoutprops.rows.length, 1);
    console.log(payoutprops);
  });

  it ("Should vote to approve role", async() => {
    await trailContract.castvote(member1.name, 0, 1, { from: member1 });
    await trailContract.castvote(member2.name, 0, 1, { from: member2 });
    await trailContract.castvote(member3.name, 0, 1, { from: member3 });

    await cuviadaoContract.closeprop (member1.name, 0, { from: member1 });

    const roles = await cuviadaoContract.provider.eos.getTableRows({
      code: cuviadaoAccount.name,
      scope: cuviadaoAccount.name,
      table: "roles",
      json: true
    });
    assert.equal(roles.rows.length, 2);
    console.log(roles);

  });
});
