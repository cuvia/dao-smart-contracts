For detailed documentation, you can see the Hypha DAO User's Guide, particularly creating a Payout Proposal: http://docs.hypha.earth/tech-docs/proposals/payouts/


To use contracts: 
```
cmake .
make
set contract
```