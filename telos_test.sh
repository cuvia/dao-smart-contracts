TEST NET: 

Private key: 5J8M2bKGXcxjhC2qX6Dmr5fhtEV3roptSzQ3n4UQoLMQrmMUxT6
Public key: EOS4www4696qSmUQT4j6z6uRJbYH1UXr42rR4tGAyL3XuanAmUiF2

cuviadaomain  -- the primary account associated with the DAO

Users:
johnnycuvia1
samanthacuvi
jamescuvia11
thomascuvia1
haydencuvia1

ryntelosuser


cleos -u https://test.telos.kitchen set contract cuviadaomain cuviadao/cuviadao

cleos -u https://test.telos.kitchen push action eosio.token transfer '["teloskitchen", "cuviadaomain", "1000.0000 TLOS", "for testing"]' -p teloskitchen
cleos -u https://test.telos.kitchen push action eosio.token transfer '["cuviadaomain", "trailservice", "1000.0000 TLOS", "deposit"]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action trailservice newtreasury '["cuviadaomain", "1000000000.0000 CUV", "public"]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action trailservice edittrsinfo '["4,CUV", "CUV Decentralized Autonomous Organization", "", ""]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action trailservice toggle '["4,CUV", "transferable"]' -p cuviadaomain

## Enrolling a user in cuvia DAO
cleos -u https://test.telos.kitchen push action trailservice regvoter '["cuviadaomain", "4,CUV", null]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action trailservice mint '["cuviadaomain", "1 CUV", "original mint"]' -p cuviadaomain

cleos -u https://test.telos.kitchen push action trailservice regvoter '["ryntelosuser", "4,CUV", null]' -p ryntelosuser
cleos -u https://test.telos.kitchen push action trailservice mint '["ryntelosuser", "1 CUV", "original mint"]' -p hyphadaomain

cleos -u https://test.telos.kitchen push action trailservice regvoter '["samanthacuvi", "4,CUV", null]' -p samanthacuvi
cleos -u https://test.telos.kitchen push action trailservice mint '["samanthacuvi", "1.0000 CUV", "original mint"]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action trailservice regvoter '["jamescuvia11", "4,CUV", null]' -p jamescuvia11
cleos -u https://test.telos.kitchen push action trailservice mint '["jamescuvia11", "1.0000 CUV", "original mint"]' -p cuviadaomain




cleos -u https://test.telos.kitchen push action trailservice castvote '["jamescuvia11", "cuvia1......2", ["pass"]]' -p jamescuvia11
cleos -u https://test.telos.kitchen push action cuviadaomain closeprop '["payouts", 0]' -p johnnycuvia1


# You can run these statements over and over because the commands end with the same state as the beginning
# The applicant must run these two actions (preferably as the same transaction)
cleos -u https://test.telos.kitchen push action trailservice regvoter '["cuvialondon2", "0,CUV", null]' -p cuvialondon2
cleos -u https://test.telos.kitchen push action cuviadaomain apply '["cuvialondon2", "I met with Debbie at the regen conference and we talked about Hypha. I would like to join."]' -p cuvialondon2

cleos -u https://test.telos.kitchen push action cuviadaomain enroll '["johnnycuvia1", "cuvialondon2", "Debbie confirmed she made this referral"]' -p johnnycuvia1

# The account can be unregistered if they transfer away their CUV and call unregvoter
cleos -u https://test.telos.kitchen push action cuviadaomain removemember '["cuvialondon2"]' -p cuviadaomain  
cleos -u https://test.telos.kitchen push action trailservice transfer '["cuvialondon2", "johnnycuvia1", "1 CUV", "memo"]' -p cuvialondon2
cleos -u https://test.telos.kitchen push action trailservice unregvoter '["cuvialondon2", "0,CUV"]' -p cuvialondon2



cleos -u https://test.telos.kitchen push action trailservice castvote '["haydencuvia1", "cuvia1.....", ["pass"]]' -p haydencuvia1
cleos -u https://test.telos.kitchen push action cuviadaomain closeprop '["payouts", 0]' -p haydencuvia1



cleos -u https://test.telos.kitchen push action cuviadaomain setlastballt '["cuvia1.....1f"]' -p cuviadaomain




## new format
cleos -u https://test.telos.kitchen push action cuviadaomain propose '{"proposer":"johnnycuvia1", 
                                                                        "proposal_type":"payout", 
                                                                        "trx_action_name":"makepayout", 
                                                                        "names":null, 
                                                                        "strings":{   
                                                                            "name":"Financial Analysis Project", 
                                                                            "description":"This project is really important and should be approved.",
                                                                            "content":"This content supports markdown for *bolding* and formatting content"
                                                                        }, 
                                                                        "assets":{
                                                                            "cuvia_amount":"4.5000 CUVIA", 
                                                                            "tlos_amount":"1.1111 TLOS"
                                                                        }, 
                                                                        "time_points":null, 
                                                                        "ints":null,
                                                                        "floats":null,
                                                                        "trxs":null' -p johnnycuvia1



##
cleos -u https://test.telos.kitchen push action cuviadaomain setconfig '["cuviatokens1", "trailservice"]' -p cuviadaomain

cleos -u https://test.telos.kitchen push action cuviatokens1 create '["cuviadaomain", "1000000000000 CUVIA"]' -p cuviatokens1
cleos -u https://test.telos.kitchen push action cuviatokens1 create '["cuviadaomain", "1000000.00000000 PRESEED"]' -p cuviatokens1
cleos -u https://test.telos.kitchen push action cuviatokens1 issue '["cuviadaomain", "1.00000000 PRESEED", "memo"]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action cuviatokens1 transfer '["cuviadaomain", "johnnycuvia1", "1.00000000 PRESEED", "memo"]' -p cuviadaomain

cleos -u https://test.telos.kitchen push action cuviadaomain addmember '["johnnycuvia1"]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action cuviadaomain addmember '["samanthahyph"]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action cuviadaomain addmember '["jamescuvia11"]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action cuviadaomain addmember '["thomascuvia1"]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action cuviadaomain addmember '["haydencuvia1"]' -p cuviadaomain


cleos -u https://test.telos.kitchen push action cuviatokens1 issue '["cuviadaomain", "1 CUVIA", "memo"]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action cuviatokens1 transfer '["cuviadaomain", "johnnycuvia1", "1 CUVIA", "memo"]' -p cuviadaomain

# cleos -u https://test.telos.kitchen push action cuviatokens1 issue '["samanthahyph", "1 CUVIA", "memo"]' -p cuviadaomain
# cleos -u https://test.telos.kitchen push action cuviatokens1 issue '["jamescuvia11", "1 CUVIA", "memo"]' -p cuviadaomain
# cleos -u https://test.telos.kitchen push action cuviatokens1 issue '["thomascuvia1", "1 CUVIA", "memo"]' -p cuviadaomain
# cleos -u https://test.telos.kitchen push action cuviatokens1 issue '["haydencuvia1", "1 CUVIA", "memo"]' -p cuviadaomain

cleos -u https://test.telos.kitchen push action eosio.trail regtoken '["1000000000000 HVOIC", "cuviadaomain", "https://dao.cuvia.earth"]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action eosio.trail issuetoken '["cuviadaomain", "johnnycuvia1", "1 HVOIC", false]' -p cuviadaomain

# cleos -u https://test.telos.kitchen push action eosio.trail issuetoken '["cuviadaomain", "cuviamember2", "1 CUV", false]' -p cuviadaomain
# cleos -u https://test.telos.kitchen push action eosio.trail issuetoken '["cuviadaomain", "cuviamember3", "1 CUV", false]' -p cuviadaomain
# cleos -u https://test.telos.kitchen push action eosio.trail issuetoken '["cuviadaomain", "cuviamember4", "1 CUV", false]' -p cuviadaomain
# cleos -u https://test.telos.kitchen push action eosio.trail issuetoken '["cuviadaomain", "cuviamember5", "1 CUV", false]' -p cuviadaomain


cleos -u https://test.telos.kitchen push action cuviadaomain proposerole '["johnnycuvia1", "Underwater Basketweaver", "Weave baskets at the bottom of the sea", "We make *great* baskets.", "11 CUVIA", "11.00000000 PRESEED", "11 CUV", 0, 10]' -p johnnycuvia1
cleos -u https://test.telos.kitchen push action cuviadaomain propassign '["johnnycuvia1", "johnnycuvia1", 0, "https://joinseeds.com", "I am a professional basket maker and scuba diver", 0, 6, 1.000000000]' -p johnnycuvia1

cleos -u https://test.telos.kitchen push action eosio.trail castvote '["johnnycuvia1", 1, 1]' -p johnnycuvia1

# cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember2", 66, 1]' -p cuviamember2
# cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember3", 66, 1]' -p cuviamember3
# cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember4", 60, 0]' -p cuviamember4

cleos -u https://test.telos.kitchen push action cuviadaomain closeprop '[0]' -p cuviamember1

cleos -u https://test.telos.kitchen push action cuviadaomain payassign '[0, 0]' -p cuviamember3
cleos -u https://test.telos.kitchen push action cuviadaomain resetperiods '[]' -p cuviadaomain
cleos -u https://test.telos.kitchen push action cuviadaomain assign '[2]' -p cuviadaomain



cleos -u https://test.telos.kitchen get table trailservice ryntelosuser voters
{
  "rows": [{
      "liquid": "1.0000 CUV",
      "staked": "0.0000 CUV",
      "staked_time": "2019-12-17T16:09:19",
      "delegated": "0.0000 CUV",
      "delegated_to": "",
      "delegation_time": "2019-12-17T16:09:19"
    }
  ],
  "more": false
}









doesnt work: 3fbdca985be8751bcfd3917bdbae80ea9e33770a
does work: 


62b2e3dd38533cd6f3ced50c3f53c76f40e98651







# Trail experimentation

cleos -u https://test.telos.kitchen get table cuviadaomain cuviadaomain applicants


cleos -u https://test.telos.kitchen get table cuviadaomain cuviadaomain config
cleos -u https://test.telos.kitchen get table cuviadaomain cuviadaomain nominees
cleos -u https://test.telos.kitchen get table cuviadaomain cuviadaomain boardmembers

cleos -u https://test.telos.kitchen get table cuviadaomain cuviadaomain proposals
cleos -u https://test.telos.kitchen get table cuviadaomain cuviadaomain roles
cleos -u https://test.telos.kitchen get table cuviadaomain cuviadaomain assignments
cleos -u https://test.telos.kitchen get table cuviadaomain cuviadaomain roleprops

cleos -u https://test.telos.kitchen get table eosio.trail eosio.trail registries
cleos -u https://test.telos.kitchen get table -lower 50  eosio.trail eosio.trail ballots
cleos -u https://test.telos.kitchen get table eosio.trail eosio.trail elections
cleos -u https://test.telos.kitchen get table eosio.trail eosio.trail leaderboards --lower 4
cleos -u https://test.telos.kitchen get table eosio.trail eosio.trail proposals

cleos -u https://test.telos.kitchen push action cuviadaobali reset '[]' -p cuviadaobali
cleos -u https://test.telos.kitchen push action cuviadaobal1 initCUV '["https://joinseeds.com"]' -p cuviadaobal1
cleos -u https://test.telos.kitchen push action cuviadaobali initsteward '["https://joinseeds.com"]' -p cuviaboard11

cleos -u https://test.telos.kitchen push action eosio.trail issuetoken '["cuviadaobal1", "cuviamember1", "1 HYVO", false]' -p cuviadaobal1
cleos -u https://test.telos.kitchen push action eosio.trail issuetoken '["cuviadaobal1", "cuviamember2", "1 HYVO", false]' -p cuviadaobal1
cleos -u https://test.telos.kitchen push action eosio.trail issuetoken '["cuviadaobal1", "cuviamember3", "1 HYVO", false]' -p cuviadaobal1
cleos -u https://test.telos.kitchen push action eosio.trail issuetoken '["cuviadaobal1", "cuviamember4", "1 HYVO", false]' -p cuviadaobal1
cleos -u https://test.telos.kitchen push action eosio.trail issuetoken '["cuviadaobal1", "cuviamember5", "1 HYVO", false]' -p cuviadaobal1

cleos -u https://test.telos.kitchen push action cuviaboard11 nominate '["cuviamember1", "cuviamember1"]' -p cuviamember1
cleos -u https://test.telos.kitchen push action cuviaboard11 makeelection '["cuviamember1", "https://joinseeds.com"]' -p cuviamember1

cleos -u https://test.telos.kitchen push action cuviaboard11 addcand '["cuviamember1", "https://joinseeds.com"]' -p cuviamember1


cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember1", 19, 0]' -p cuviamember1
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember2", 19, 1]' -p cuviamember2
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember3", 19, 1]' -p cuviamember3
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember4", 19, 1]' -p cuviamember4

cleos -u https://test.telos.kitchen push action cuviaboard11 endelection '["cuviamember1"]' -p cuviamember1


##### Propose a role
cleos -u https://test.telos.kitchen push action cuviadaobal1 proposerole '["cuviamember1", "Strawberry Gatherer", "https://joinseeds.com", "Farmer growing food", "12 CUVIA", "9 PRESEED", "15 HYVOICE"]' -p cuviamember1
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember4", 41, 1]' -p cuviamember4
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember2", 41, 1]' -p cuviamember2
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember3", 41, 1]' -p cuviamember3
cleos -u https://test.telos.kitchen push action cuviadaobal1 closeprop '["cuviamember1", 0]' -p cuviamember1

#####  Propose an assignment
cleos -u https://test.telos.kitchen push action cuviadaobal1 propassign '["cuviamember1", "cuviamember1", 0, "https://joinseeds.com", "I would like this job", 0, 1.000000000]' -p cuviamember1
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember1", 34, 1]' -p cuviamember1
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember2", 34, 1]' -p cuviamember2
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember3", 34, 1]' -p cuviamember3
cleos -u https://test.telos.kitchen push action cuviadaobal1 closeprop '[1]' -p cuviamember1

##### Propose a contribution
cleos -u https://test.telos.kitchen set contract cuviadaobal1 cuviadao/cuviadao
cleos -u https://test.telos.kitchen push action cuviadaobal1 proppayout '["cuviamember2", "cuviamember2", "Investment", "https://joinseeds.com", "2000 HHH", "10000.00000000 PPP", "5 HYVO", "2019-07-05T05:49:01.500"]' -p cuviamember2
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember1", 50, 1]' -p cuviamember1
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember2", 50, 1]' -p cuviamember2
cleos -u https://test.telos.kitchen push action eosio.trail castvote '["cuviamember3", 50, 1]' -p cuviamember3
cleos -u https://test.telos.kitchen push action cuviadaobal1 closeprop '["cuviamember2", 9]' -p cuviamember2

cleos -u https://test.telos.kitchen get table cuviatokens1 cuviamember2 accounts

cleos -u https://test.telos.kitchen push action cuviadaobal1 makepayout '[2]' -p cuviadaobal1
cleos -u https://test.telos.kitchen push action cuviadaobal1 reset '[]' -p cuviadaobal1





cleos -u https://test.telos.kitchen push action cuviadaobali setconfig '["cuviatokens1", "cuviatokens1"]' -p cuviadaobali


cleos -u https://test.telos.kitchen push action eosio updateauth '{
    "account": "cuviadaomain",
    "permission": "owner",
    "parent": "",
    "auth": {
        "keys": [
            {
                "key": "EOS5tEdJd32ANvoxSecRnY5ucr1jbzaVN2rQZegj6NxsevGU8JoaJ",
                "weight": 1
            }
        ],
        "threshold": 1,
        "accounts": [
            {
                "permission": {
                    "actor": "cuviadaomain",
                    "permission": "eosio.code"
                },
                "weight": 1
            }
        ],
        "waits": []
    }
}' -p cuviadaomain@owner

cleos -u https://test.telos.kitchen push action eosio updateauth '{
    "account": "cuviadaomain",
    "permission": "active",
    "parent": "owner",
    "auth": {
        "keys": [
            {
                "key": "EOS5tEdJd32ANvoxSecRnY5ucr1jbzaVN2rQZegj6NxsevGU8JoaJ",
                "weight": 1
            }
        ],
        "threshold": 1,
        "accounts": [
            {
                "permission": {
                    "actor": "cuviadaomain",
                    "permission": "eosio.code"
                },
                "weight": 1
            }
        ],
        "waits": []
    }
}' -p cuviadaomain@owner




------------------------------------------------------------------------------------------------
OLD TEST NET: 

Private key: 5J1gYLAc4GUo7EXNAXyaZTgo3m3SxtxDygdVUsNL4Par5Swfy1q
Public key: EOS6vvAofsMC5RJyY6fRHcyiQLNjDGukX6tRUoF1WEc63idQ3BqJn

cuviadaotest
cuviatoken11
cuviaboard11
cuviaboard11
cuviamember1
cuviamember2
cuviamember3
cuviamember4
cuviamember5
cuviadaobali
cuviatokens1
