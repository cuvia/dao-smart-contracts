#include <cuviadao.hpp>

void cuviadao::addmember (const name& member) {
	require_auth (get_self());
	member_table m_t (get_self(), get_self().value);
	auto m_itr = m_t.find (member.value);
	check (m_itr == m_t.end(), "Account is already a member: " + member.to_string());
	m_t.emplace (get_self(), [&](auto &m) {
		m.member = member;
	});
}

void cuviadao::removemember (const name& member) {
	require_auth (get_self());
	member_table m_t (get_self(), get_self().value);
	auto m_itr = m_t.find (member.value);
	check (m_itr != m_t.end(), "Account is not a member: " + member.to_string());
	m_t.erase (m_itr);
}

void cuviadao::reset () {

	proposal_table p_t (get_self(), "payouts"_n.value);
	auto p_itr = p_t.begin();
	while (p_itr != p_t.end()) {
		p_itr = p_t.erase (p_itr);
	}

	// config_table      config_s (get_self(), get_self().value);
   	// DAOConfig c = config_s.get_or_create (get_self(), DAOConfig()); 
	// config_s.remove ();
}

void cuviadao::settrail ( const name& 	trail_contract) {

   	require_auth (get_self());
	config_table      config_s (get_self(), get_self().value);
   	DAOConfig c = config_s.get_or_create (get_self(), DAOConfig());   
	c.trail_contract 	= trail_contract;
	config_s.set (c, get_self());
}

void cuviadao::setlastballt ( const name& last_ballot_id) {
	require_auth (get_self());
	config_table      config_s (get_self(), get_self().value);
   	DAOConfig c = config_s.get_or_create (get_self(), DAOConfig());   
	c.last_ballot_id 	= last_ballot_id;
	config_s.set (c, get_self());
}

void cuviadao::enroll (const name& enroller,
						const name& applicant, 
						const string& content) {

	// this action is linked to the cuviadaomain@enrollers permission
	
	applicant_table a_t (get_self(), get_self().value);
	auto a_itr = a_t.find (applicant.value);
	check (a_itr != a_t.end(), "Applicant not found: " + applicant.to_string());

	config_table      config_s (get_self(), get_self().value);
   	DAOConfig c = config_s.get_or_create (get_self(), DAOConfig());  

	asset one_cuv = asset { 10000, S_CUV  };
	string memo { "Welcome to Cuvia DAO!"};
	action(	
		permission_level{get_self(), "active"_n}, 
		c.trail_contract, "mint"_n, 
		make_tuple(applicant, one_cuv, memo))
	.send();

	// Should we also send 1 CUVIA?  
	// asset one_cuvia = asset { 1, common::S_CUVIA };
	// bank.makepayment (-1, applicant, one_cuvia, memo, common::NO_ASSIGNMENT);

	member_table m_t (get_self(), get_self().value);
	auto m_itr = m_t.find (applicant.value);
	check (m_itr == m_t.end(), "Account is already a member: " + applicant.to_string());
	m_t.emplace (get_self(), [&](auto &m) {
		m.member = applicant;
	});

	a_t.erase (a_itr);
}	

void cuviadao::apply (const name& applicant, 
						const string& content) {

	require_auth (applicant);
	applicant_table a_t (get_self(), get_self().value);
	auto a_itr = a_t.find (applicant.value);

	if (a_itr != a_t.end()) {
		a_t.modify (a_itr, get_self(), [&](auto &a) {
			a.content = content;
			a.updated_date = current_time_point();
		});
	} else {
		a_t.emplace (get_self(), [&](auto &a) {
			a.applicant = applicant;
			a.content = content;
		});
	}
}				

name cuviadao::register_ballot (const name& proposer, 
								const map<string, string>& strings) 
{
	require_auth(proposer);
	qualify_proposer(proposer);

	config_table      config_s (get_self(), get_self().value);
   	DAOConfig c = config_s.get_or_create (get_self(), DAOConfig());  
	
	// increment the ballot_id
	name new_ballot_id = name (c.last_ballot_id.value + 1);

	c.last_ballot_id = new_ballot_id;
	config_s.set(c, get_self());
	
	trailservice::trail::ballots_table b_t ("trailservice"_n, "trailservice"_n.value);
	auto b_itr = b_t.find (new_ballot_id.value);
	check (b_itr == b_t.end(), "ballot_id: " + new_ballot_id.to_string() + " has already been used.");

	vector<name> options;
   	options.push_back ("pass"_n);
   	options.push_back ("fail"_n);

	action (
      permission_level{get_self(), "active"_n},
      c.trail_contract, "newballot"_n,
      std::make_tuple(
			new_ballot_id, 
			"poll"_n, 
			get_self(), 
			S_CUV, 
			"1token1vote"_n, 
			options))
   .send();

//    action (
//       permission_level{get_self(), "active"_n},
//       c.trail_contract, "togglebal"_n,
//       std::make_tuple(new_ballot_id, "votestake"_n))
//    .send();

   action (
	   	permission_level{get_self(), "active"_n},
		c.trail_contract, "editdetails"_n,
		std::make_tuple(
			new_ballot_id, 
			strings.at("title"), 
			strings.at("description"),
			strings.at("content")))
   .send();

   auto expiration = time_point_sec(current_time_point()) + 65;
   
   action (
      permission_level{get_self(), "active"_n},
      c.trail_contract, "openvoting"_n,
      std::make_tuple(new_ballot_id, expiration))
   .send();

	return new_ballot_id;
}

void cuviadao::propose (const name&                		proposer, 
						const name&                		proposal_type,
						const std::optional<name>&      trx_action_name,
						const map<string, name> 		names,
						const map<string, string>       strings,
						const map<string, asset>        assets,
						const map<string, time_point>   time_points,
						const map<string, uint64_t>     ints,
						const map<string, float>        floats,
						const map<string, transaction>  trxs)
{
	require_auth (proposer);
	qualify_proposer (proposer);

	proposal_table p_t (get_self(), proposal_type.value);
	p_t.emplace (get_self(), [&](auto &p) {
		p.id                       	= p_t.available_primary_key();
		p.proposer                 	= proposer;

		p.names                    	= names;
		p.names["proposal_type"]   	= proposal_type;
		p.names["ballot_id"]		= register_ballot (proposer, strings);

		p.strings                  	= strings;
		p.assets                  	= assets;
		p.time_points              	= time_points;
		p.ints                     	= ints;
		p.floats                   	= floats;
		p.trxs                     	= trxs;

		// this transaction executes if the proposal passes
		transaction trx (time_point_sec(current_time_point())+ (60 * 60 * 24 * 35));
		trx.actions.emplace_back(
			permission_level{get_self(), "active"_n}, 
			get_self(), trx_action_name.value(), 
			std::make_tuple(p.id));
		trx.delay_sec = 0;
		p.trxs["transaction"]      = trx;      
	});      
}

void cuviadao::makepayout (const uint64_t&        proposal_id) {

	debug ("Inside of makepayout");
	require_auth (get_self());

	name proposal_type { "payouts" };
	debug ("Proposal type: " + proposal_type.to_string());
	debug ("Proposal ID: " + std::to_string(proposal_id));

	proposal_table p_t (get_self(), proposal_type.value);
	auto p_itr = p_t.find (proposal_id);
	check (p_itr != p_t.end(), "Proposal Type: " + proposal_type.to_string() + "; ID: " + std::to_string(proposal_id) + " does not exist.");

	string memo { "Payment from Cuvia DAO: " + std::to_string(proposal_id) };
	debug (" CUVIA payout: " + p_itr->assets.at("cuvia_amount").to_string());

	// if there is a TLOS value in the proposal, send it
	if (p_itr->assets.at("tlos_amount").amount > 0) {
		debug ("Making a payment to : " + p_itr->names.at("recipient").to_string());
		action(	
			permission_level{get_self(), "active"_n}, 
			"eosio.token"_n, "transfer"_n, 
			make_tuple(get_self(), p_itr->names.at("recipient"), p_itr->assets.at("tlos_amount"), memo
		)).send();
	}

	// if there is a CUV value in the proposal, that gets minted from Trail
	if (p_itr->assets.at("cuvia_amount").amount > 0) {
		config_table      config_s (get_self(), get_self().value);
   		DAOConfig c = config_s.get_or_create (get_self(), DAOConfig());  

		action(	
			permission_level{get_self(), "active"_n}, 
			c.trail_contract, "mint"_n, 
			make_tuple(p_itr->names.at("recipient"), p_itr->assets.at("cuvia_amount"), memo
		)).send();
	}
}

void cuviadao::eraseprop (const name& proposal_type, const uint64_t& proposal_id) {
	require_auth (get_self());
	proposal_table props(get_self(), proposal_type.value);
	auto i_iter = props.find(proposal_id);
	check(i_iter != props.end(), "prop not found");
	props.erase (i_iter);
}

void cuviadao::closeprop(const name& proposal_type,
						 const uint64_t& proposal_id) {

	proposal_table props(get_self(), proposal_type.value);
	auto i_iter = props.find(proposal_id);
	check(i_iter != props.end(), "prop not found");
	auto prop = *i_iter;

	config_table      config_s (get_self(), get_self().value);
   	DAOConfig c = config_s.get_or_create (get_self(), DAOConfig());  

	trailservice::trail::ballots_table b_t (c.trail_contract, c.trail_contract.value);
	auto b_itr = b_t.find (prop.names.at("ballot_id").value);
	check (b_itr != b_t.end(), "ballot_id: " + prop.names.at("ballot_id").to_string() + " not found.");

	string debug_str = string ("Ballot ID read from prop for closing ballot: " + prop.names.at("ballot_id").to_string() + "\n");
	// action (
	// 	permission_level{get_self(), "active"_n},
	// 	c.trail_contract, "closevoting"_n,
	// 	std::make_tuple(prop.names.at("ballot_id"), true))
	// .send();

	uint32_t quorum_threshold = b_itr->total_voters / (5 - 1);
	debug_str = debug_str + "Quorum threshold: " + std::to_string(quorum_threshold) + "\n";

	map<name, asset> votes = b_itr->options;
	asset votes_pass = votes.at("pass"_n);
	asset votes_fail = votes.at("fail"_n);
	debug_str = debug_str + "Votes passing: " + votes_pass.to_string() + "\n";
	debug_str = debug_str + "Votes failing: " + votes_fail.to_string() + "\n";

	if (b_itr->total_voters > quorum_threshold && votes_pass > votes_fail) {
		debug_str = debug_str + "Executing transaction\n";
		eosio::transaction trx = prop.trxs.at("transaction");
		debug ( "Action name in transaction: " + trx.actions[0].name.to_string());
		debug ( "Contract name in transaction: " + trx.actions[0].account.to_string());
		// debug ( "")
		// prop.trxs.at("transaction")
		trx.send(current_block_time().to_time_point().sec_since_epoch(), get_self());		
	}

	debug (debug_str);
	archive (prop);
	props.erase(i_iter);
}

void cuviadao::qualify_proposer (const name& proposer) {
	//check (bank.holds_cuvia (proposer), "Proposer: " + proposer.to_string() + " does not hold CUVIA.");
}